import sys
## Your Project PATH
sys.path.append("/PATH/TO/YOUR/PROJECT/")
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from salestock_qa_test.utils.base import Base


class NavigationTop(Base):

	def get_signin_button(self):
		return  WebDriverWait(self.driver, 10).until(
			EC.element_to_be_clickable((By.XPATH, "//a[contains(text(), \"Sign in\")]")))

	def get_contactus_button(self):
		return  WebDriverWait(self.driver, 10).until(
			EC.element_to_be_clickable((By.XPATH, "//a[contains(text(), \"Contact\")]")))

	def get_search_field(self):
		return  WebDriverWait(self.driver, 10).until(
			EC.element_to_be_clickable((By.ID, "search_query_top")))

	def get_search_button(self):
		return  WebDriverWait(self.driver, 10).until(
			EC.element_to_be_clickable((By.ID, "submit_search")))

	def set_search_field(self, keyword):
		search_field = self.get_search_field()
		search_field.clear()
		search_field.send_keys(keyword)


class PromotionTop(Base):

	def get_slider_product(self):
		return  WebDriverWait(self.driver, 10).until(
			EC.element_to_be_clickable((By.ID, "homepage-slider")))

	def get_slider_shopnow_button(self):
		return  WebDriverWait(self.driver, 10).until(
			EC.element_to_be_clickable((By.XPATH, "//button[contains(text(), \"Shop now\")]")))
	
	def get_sale_product_mid1(self):
		return  WebDriverWait(self.driver, 10).until(
			EC.element_to_be_clickable((By.CLASS_NAME, "htmlcontent-item-1 col-xs-4")))

	def get_sale_product_mid2(self):
		return  WebDriverWait(self.driver, 10).until(
			EC.element_to_be_clickable((By.CLASS_NAME, "htmlcontent-item-2 col-xs-4")))
