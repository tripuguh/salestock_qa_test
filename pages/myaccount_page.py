import sys
## Your Project PATH
sys.path.append("/PATH/TO/YOUR/PROJECT/")

from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

from salestock_qa_test.utils.base import Base


class MyAccount(Base):

	def get_myaccount_heading(self):
		return  WebDriverWait(self.driver, 10).until(
			EC.presence_of_element_located((By.XPATH, "//h1[contains(text(), \"My account\")]")))

	def get_myaccount_signout_button(self):
		return  WebDriverWait(self.driver, 10).until(
			EC.presence_of_element_located((By.XPATH, "//a[@title=\"Log me out\"]")))
