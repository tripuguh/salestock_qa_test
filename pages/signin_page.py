import sys
## Your Project PATH
sys.path.append("/PATH/TO/YOUR/PROJECT/")

from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

from salestock_qa_test.utils.base import Base


class CreateAccount(Base):

	def get_create_account_form(self):
		return  WebDriverWait(self.driver, 10).until(
			EC.presence_of_element_located((By.ID, "create-account_form")))

	def get_create_account_email_field(self):
		return  WebDriverWait(self.driver, 10).until(
			EC.presence_of_element_located((By.ID, "email_create")))

	def get_create_account_button(self):
		return  WebDriverWait(self.driver, 10).until(
			EC.element_to_be_clickable((By.ID, "SubmitCreate")))

	def get_create_account_invalid_email_message(self):
		return  WebDriverWait(self.driver, 10).until(
			EC.presence_of_element_located((By.XPATH, "//li[contains(text(),\"Invalid email\")]")))

	def get_create_account_registered_email_message(self):
		return  WebDriverWait(self.driver, 10).until(
			EC.presence_of_element_located((By.XPATH, "//li[contains(text(),\"An account\")]")))

	def set_create_account_email_field(self, email):
		email_field = self.get_create_account_email_field()
		email_field.clear()
		email_field.send_keys(email)



class SignIn(Base):

	def get_signin_form(self):
		return  WebDriverWait(self.driver, 10).until(
			EC.presence_of_element_located((By.ID, "login_form")))

	def get_signin_email_field(self):
		return  WebDriverWait(self.driver, 10).until(
			EC.presence_of_element_located((By.ID, "email")))

	def get_signin_password_field(self):
		return  WebDriverWait(self.driver, 10).until(
			EC.presence_of_element_located((By.ID, "passwd")))

	def get_signin_button(self):
		return  WebDriverWait(self.driver, 10).until(
			EC.element_to_be_clickable((By.ID, "SubmitLogin")))

	def get_signin_email_required_message(self):
		return WebDriverWait(self.driver, 10).until(
			EC.presence_of_element_located((By.XPATH, "//li[contains(text(), \"An email\")]")))

	def get_signin_invalid_email_message(self):
		return WebDriverWait(self.driver, 10).until(
			EC.presence_of_element_located((By.XPATH, "//li[contains(text(), \"Invalid email\")]")))

	def get_signin_password_required_message(self):
		return WebDriverWait(self.driver, 10).until(
			EC.presence_of_element_located((By.XPATH, "//li[contains(text(), \"Password is\")]")))

	def get_signin_invalid_password_message(self):
		return WebDriverWait(self.driver, 10).until(
			EC.presence_of_element_located((By.XPATH, "//li[contains(text(), \"Invalid password\")]")))

	def get_signin_auth_failed(self):
		return WebDriverWait(self.driver, 10).until(
			EC.presence_of_element_located((By.XPATH, "//li[contains(text(), \"Authentication failed\")]")))

	def set_signin_email_field(self, email):
		email_field = self.get_signin_email_field()
		email_field.clear()
		email_field.send_keys(email)

	def set_signin_password_field(self, password):
		email_field = self.get_signin_password_field()
		email_field.clear()
		email_field.send_keys(password)