import sys
## Your Project PATH
sys.path.append("/PATH/TO/YOUR/PROJECT/")

from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

from salestock_qa_test.utils.base import Base

class CreateAccountForm(Base):

	def get_account_creation_form(self):
		return  WebDriverWait(self.driver, 10).until(
			EC.presence_of_element_located((By.ID, "account-creation_form")))
