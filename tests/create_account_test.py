import sys
## Your Project PATH
sys.path.append("/PATH/TO/YOUR/PROJECT/")

import unittest
from selenium import webdriver
from salestock_qa_test.pages.home_page import NavigationTop
from salestock_qa_test.pages.signin_page import CreateAccount
from salestock_qa_test.pages.create_account_form_page import CreateAccountForm


class CreateAccountTest(unittest.TestCase):

	def setUp(self):
		## Your chromedriver PATH
		chromedriver = "/PATH/TO/YOUR/CHROMEDRIVER"
		self.driver = webdriver.Chrome(chromedriver)
		self.driver.get("http://automationpractice.com")
		self.nav_top = NavigationTop(self.driver)
		self.create_account = CreateAccount(self.driver)
		self.account_form = CreateAccountForm(self.driver)

		## 1. Create account without email
	def test_create_account_without_email(self):
		self.nav_top.get_signin_button().click()
		self.create_account.set_create_account_email_field("")
		self.create_account.get_create_account_button().click()
		self.assertTrue(self.create_account.get_create_account_invalid_email_message().is_displayed())

		## 2. Create account with invalid email format
	def test_create_account_with_invalid_email(self):
		self.nav_top.get_signin_button().click()
		self.create_account.set_create_account_email_field("email@email")
		self.create_account.get_create_account_button().click()
		self.assertTrue(self.create_account.get_create_account_invalid_email_message().is_displayed())

		## 3. Create email with registered email
	def test_create_account_with_registered_email(self):
		self.nav_top.get_signin_button().click()
		self.create_account.set_create_account_email_field("salestock_qa@gmail.com")
		self.create_account.get_create_account_button().click()
		self.assertTrue(self.create_account.get_create_account_registered_email_message().is_displayed())

		## 4. Create account with valid email format
	def test_create_account_with_valid_email(self):
		self.nav_top.get_signin_button().click()
		self.create_account.set_create_account_email_field("baru@gmail.com")
		self.create_account.get_create_account_button().click()
		self.assertTrue(self.account_form.get_account_creation_form().is_displayed())

	def tearDown(self):
		self.driver.close()
		self.driver.quit()


if __name__ == "__main__":
	log_file = "create_account_test.log"
	f = open(log_file, "w")
	runner = unittest.TextTestRunner(f, verbosity=2)
	unittest.main(testRunner=runner)
	f.close()

