import sys
## Your Project PATH
sys.path.append("/PATH/TO/YOUR/PROJECT/")
import unittest
from selenium import webdriver
from salestock_qa_test.pages.home_page import NavigationTop
from salestock_qa_test.pages.signin_page import SignIn
from salestock_qa_test.pages.myaccount_page import MyAccount


class SignInTest(unittest.TestCase):

	def setUp(self):
		## Your chromedriver PATH
		chromedriver = "/PATH/TO/YOUR/CHROMEDRIVER"
		self.driver = webdriver.Chrome(chromedriver)
		self.driver.get("http://automationpractice.com")
		self.nav_top = NavigationTop(self.driver)
		self.signin = SignIn(self.driver)
		self.myaccount = MyAccount(self.driver)

		## 1. Sign in without email and password
	def test_signin_without_email_n_pass(self):
		self.nav_top.get_signin_button().click()
		self.signin.set_signin_email_field("")
		self.signin.set_signin_password_field("")
		self.signin.get_signin_button().click()
		self.assertTrue(self.signin.get_signin_email_required_message().is_displayed())

		## 2. Sign in with invalid email and empty password
	def test_signin_invalidemail_nopass(self):
		self.nav_top.get_signin_button().click()
		self.signin.set_signin_email_field("email@email")
		self.signin.set_signin_password_field("")
		self.signin.get_signin_button().click()
		self.assertTrue(self.signin.get_signin_invalid_email_message().is_displayed())

		## 3. Sign in with invalid email and invalid password
	def test_signin_invalidemail_invalidpass(self):
		self.nav_top.get_signin_button().click()
		self.signin.set_signin_email_field("email@email")
		self.signin.set_signin_password_field("oo")
		self.signin.get_signin_button().click()
		self.assertTrue(self.signin.get_signin_invalid_email_message().is_displayed())

		## 4. Sign in with invalid email and valid password
	def test_signin_invalidemail_validpass(self):
		self.nav_top.get_signin_button().click()
		self.signin.set_signin_email_field("email@email")
		self.signin.set_signin_password_field("salestock123")
		self.signin.get_signin_button().click()
		self.assertTrue(self.signin.get_signin_invalid_email_message().is_displayed())

		## 5. Sign in with empty email and invalid password
	def test_signin_noemail_invalidpass(self):
		self.nav_top.get_signin_button().click()
		self.signin.set_signin_email_field("")
		self.signin.set_signin_password_field("oo")
		self.signin.get_signin_button().click()
		self.assertTrue(self.signin.get_signin_email_required_message().is_displayed())

		## 6. Sign in with empty email and valid password
	def test_signin_noemail_validpass(self):
		self.nav_top.get_signin_button().click()
		self.signin.set_signin_email_field("")
		self.signin.set_signin_password_field("salestock123")
		self.signin.get_signin_button().click()
		self.assertTrue(self.signin.get_signin_email_required_message().is_displayed())

		## 7. Sign in with valid email and no password
	def test_signin_validemail_nopass(self):
		self.nav_top.get_signin_button().click()
		self.signin.set_signin_email_field("salestock_qa@gmail.com")
		self.signin.set_signin_password_field("")
		self.signin.get_signin_button().click()
		self.assertTrue(self.signin.get_signin_password_required_message().is_displayed())

		## 8. Sign in with valid email and invalid password
	def test_signin_validemail_invalidpass(self):
		self.nav_top.get_signin_button().click()
		self.signin.set_signin_email_field("salestock_qa@gmail.com")
		self.signin.set_signin_password_field("oo")
		self.signin.get_signin_button().click()
		self.assertTrue(self.signin.get_signin_invalid_password_message().is_displayed())

		## 9. Sign in with valid email and valid password
	def test_signin_validemail_validpass(self):
		self.nav_top.get_signin_button().click()
		self.signin.set_signin_email_field("salestock_qa@gmail.com")
		self.signin.set_signin_password_field("salestock123")
		self.signin.get_signin_button().click()
		self.assertTrue(self.myaccount.get_myaccount_heading().is_displayed())
		self.assertTrue(self.myaccount.get_myaccount_signout_button().is_displayed())

	def tearDown(self):
		self.driver.close()
		self.driver.quit()


if __name__ == "__main__":
	log_file = "signin_test.log"
	f = open(log_file, "w")
	runner = unittest.TextTestRunner(f, verbosity=2)
	unittest.main(testRunner=runner)
	f.close()
